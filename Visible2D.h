#pragma once

#include "Visible/Visible.h"
#include "Messenger/Listener.h"
#include "Messenger/Message.h"

class CVisible2D : public CVisible, public CListener
{
public:
			CVisible2D();
			~CVisible2D() override;
	void	Push( CMessage& ) override;
};
