#include "Visible2D.h"
#include <assert.h>
#include "Window/Window.h"
#include "Messenger/Messenger.h"

CVisible2D::CVisible2D()
{
	assert( CWindow::GetDefaultWindow() );
	CMessenger::GlobalListen( *this, CWindow::GetDefaultWindow()->GetDraw2DMessage() );
}

CVisible2D::~CVisible2D()
{
	assert( CWindow::GetDefaultWindow() );
	CMessenger::GlobalStopListening( *this, CWindow::GetDefaultWindow()->GetDraw2DMessage() );
}

void CVisible2D::Push( CMessage& rMessage )
{
	if (rMessage.GetType() == CWindow::GetDefaultWindow()->GetDraw2DMessage())
	{
		Draw();
	}
}
